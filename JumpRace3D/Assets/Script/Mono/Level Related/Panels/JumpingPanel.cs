using System.Collections;
using UnityEngine;

public class JumpingPanel : PathItemBase
{
    [SerializeField] public int PanelIndex;

    [SerializeField] public bool IsSupporter;
    [SerializeField] public bool IsSpecial;
    [SerializeField] public Transform Center;
    [SerializeField] private float _disappearDelayTime;
    [SerializeField] private float _wavingTime;

    [SerializeField] private MeshRenderer _panelMesh;

    [SerializeField] private float _waveSpeed;
    [SerializeField] private float _waveScele;
    [SerializeField] private GameObject _fireObject;
    [SerializeField] private float _fireTime;

    public void ShowWave()
    {

        _panelMesh.material.SetFloat(MaterialKeys.XSpeed, _waveSpeed);
        _panelMesh.material.SetFloat(MaterialKeys.YSpeed, _waveSpeed);
        _panelMesh.material.SetFloat(MaterialKeys.Scale, _waveScele);
        


        StartCoroutine(EndWaving());
    }


    public void ShowFireUpEffect()
    {
        _fireObject.SetActive(true);
    }

    public void Disappear()
    {
        StartCoroutine(DisappearDelayed());
    }


    private IEnumerator EndWaving()
    {
        yield return new WaitForSeconds(_wavingTime);

        _panelMesh.materials[0].SetFloat(MaterialKeys.XSpeed, 0);
        _panelMesh.materials[0].SetFloat(MaterialKeys.YSpeed, 0);
        _panelMesh.material.SetFloat(MaterialKeys.Scale, 0);

    }




    private IEnumerator Disapear()
    {
        yield return new WaitForSeconds(_fireTime);
        gameObject.SetActive(false);

    }




 
    private IEnumerator DisappearDelayed()
    {
        yield return new WaitForSeconds(_disappearDelayTime);
        gameObject.SetActive(false);
    }



}
